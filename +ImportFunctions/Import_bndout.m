function [t,RF] = Import_bndout(path)
% Function for importing the reaction forces on velocity/displacemenbt
% boundary conditions in LS-DYNA
%
% -------------------------------- INPUT -------------------------------- %
% path                          String containing the full path of the
%                               bndout-file
%
% -------------------------------- OUTPUT ------------------------------- %
% RF                            Struct containing all the reported reaction
%                               forces
%
%
% ----------------------------------------------------------------------- %
%
% 05.07.2021 NIFD


t = zeros(1,10000);
RF = zeros(3,length(t));

%% Import data
% Open file path
fID = fopen(path);

% Read data line by line
index = 0;
tline = fgetl(fID);
tic
while ischar(tline)
    
    if contains(tline,' n o d a l   f o r c e/e n e r g y    o u t p u t  t=')
        % Save time point in temp. var. 
        temp = str2double(tline(end-11:end));

        % Jump over single line
        fgetl(fID);
        % Load next line
        tline = fgetl(fID);

        if contains(tline,'velocity')
            index = index + 1;
            t(index) = temp;
            
            % Jump over empty line
            fgetl(fID);
            
            % Load next line
            tline = fgetl(fID);
            
            
            
            while ~contains(tline,'xtotal')
                
                
                tline = fgetl(fID);
                
            end
%             keyboard
            RF(1,index) = str2double(tline(24:34));
            RF(2,index) = str2double(tline(48:58));
            RF(3,index) = str2double(tline(70:80));
            
            

        end
            

    end

    tline = fgetl(fID);
end
toc
% Close file
fclose(fID);

%%
t = t(1:find(t,1,'last'));
RF = RF(:,1:find(t,1,'last'));

