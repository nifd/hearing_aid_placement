function [Res] = Import_matsum(path)
% Function for importing the local energies reported in the matsum-file
% from LS-DYNA.
%
% -------------------------------- INPUT -------------------------------- %
% path                          String containing the full path of the
%                               matsum-file
%
% -------------------------------- OUTPUT ------------------------------- %
% Res                           Result struct with all information from the
%                               matsum file
%
% ----------------------------------------------------------------------- %
%
% 05.07.2021 NIFD

%% Preallocate arrays

t = zeros(1,10000);
EnergyNames = {'Internal','Kinetic','ErodedInternal','ErodedKinetic','Hourglass'};
Dirs = 'XYZ';
%% Import data
% Open file path
fID = fopen(path);

% Read data line by line
index = 0;
tline = fgetl(fID);
while ischar(tline)
    % Identify part/# key
    if contains(tline,'{BEGIN LEGEND}')
        % Jump through 2 lines
        tline = fgetl(fID); tline = fgetl(fID);
        
        while ~contains(tline,'{END LEGEND}')
            idNo = str2double(tline(1:20));
            idBackslash = strfind(tline,'\');
            tempNam = tline(idBackslash(end)+1:end);
            % Remove Whitespaces
            tempNam = tempNam(~isspace(tempNam));
            MatNam{idNo} = tempNam;
            
            % Preallocate struc arrays
            for eNam = 1:numel(EnergyNames)
                Res.(MatNam{idNo}).Energy.(EnergyNames{eNam}) = t;
            end
            
            for iDir = 1:numel(Dirs)
                Res.(MatNam{idNo}).Moment.(Dirs(iDir)) = t;
                Res.(MatNam{idNo}).Velocity.(Dirs(iDir)) = t;
            end
            % Step on
            tline = fgetl(fID);
        end
    end
    
    % Identify if new time step block has been reached
    if contains(tline,'time =')
        index = index+1;
        t(index) = str2double(tline(11:end));
    end
    
    if contains(tline,'mat.#=')
        % Identify material key and assign energy values
        idNo = str2double(tline(8:25));
        Res.(MatNam{idNo}).Energy.Internal(index) = str2double(tline(32:49));
        Res.(MatNam{idNo}).Energy.Kinetic(index) = str2double(tline(56:73));
        Res.(MatNam{idNo}).Energy.ErodedInternal(index) = str2double(tline(84:101));
        Res.(MatNam{idNo}).Energy.ErodedKinetic(index) = str2double(tline(112:124));
        
        %Next line
        tline = fgetl(fID);
        
        % Moments
        Res.(MatNam{idNo}).Moment.X(index) = str2double(tline(8:25));
        Res.(MatNam{idNo}).Moment.Y(index) = str2double(tline(32:49));
        Res.(MatNam{idNo}).Moment.Z(index) = str2double(tline(56:68));
        
        % Next line
        tline = fgetl(fID);
        
        % Rigid body velocities
        Res.(MatNam{idNo}).Velocity.X(index) = str2double(tline(8:25));
        Res.(MatNam{idNo}).Velocity.Y(index) = str2double(tline(32:49));
        Res.(MatNam{idNo}).Velocity.Z(index) = str2double(tline(56:68));
        
        % Next line
        tline = fgetl(fID);
        
        % Hourglass energy
        Res.(MatNam{idNo}).Energy.Hourglass(index) = str2double(tline(end-10:end));
    end
    
    
    % Next line
    tline = fgetl(fID);
    
end

% Close file
fclose(fID);

% Remove trailing zeros
idEnd = find(t,1,'last');
Res.time = t(1:idEnd);

for iMat = 1:numel(MatNam)
    for iEne = 1:numel(EnergyNames)
        Res.(MatNam{iMat}).Energy.(EnergyNames{iEne}) = ...
            Res.(MatNam{iMat}).Energy.(EnergyNames{iEne})(1:idEnd);
    end
    
    for iDir = 1:numel(iDir)
        Res.(MatNam{iMat}).Moment.(Dirs(iDir)) = ...
            Res.(MatNam{iMat}).Moment.(Dirs(iDir))(1:idEnd);
    end
end



