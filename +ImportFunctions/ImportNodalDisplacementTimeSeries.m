function [times,Def] = ImportNodalDisplacementTimeSeries(path)
% Function for importing the reults at the nodes exported from ANSYS using
% the "TimeSeries" ACT code in the Automation Tab
%
% -------------------------------- INPUT -------------------------------- %
% path                          String containing the full path of the
%                               data. The data must be in the form of a
%                               tab-separated list containing 5 columns:
%                               [node no., x0, y0, z0, directional
%                               deformation]. They must be named according
%                               to the sampling time
%
% -------------------------------- OUTPUT ------------------------------- %
% Def                           N_node x N_time array containing the
%                               directional deformation. First row is the
%                               node numbers while subsequent rows are the
%                               specific coordinate for the centroid at
%                               that timestep.
%
% ----------------------------------------------------------------------- %

%% Folder content
FoldCont = dir(path);
N_files = numel(FoldCont)-2;



%% Import data

% Sort data files according to increasing time
times = zeros(1,N_files);

for iFile = 1:N_files
filename =  FoldCont(2+iFile).name;
% Indices containing time stamp
[startInd,endInd] = regexp(filename,'\d*');
timestamp = str2double(filename(startInd(1):endInd(end)));
times(iFile) = timestamp;
end
[times,indSort] = sort(times);

% Import data in ascending order
for iFile = 1:N_files
fileloc = [path '\' FoldCont(2+indSort(iFile)).name];

tempRes = ImportFunctions.ImportNodalDirectionalResults(fileloc);

if iFile == 1
    [rows,cols] = size(tempRes);
    
    DirRes = zeros(rows,cols+N_files-1);
    
    DirRes(:,1:5) = tempRes(:,1:5);
else
    DirRes(:,4+iFile) = tempRes(:,end);
end

end

Def = DirRes;
