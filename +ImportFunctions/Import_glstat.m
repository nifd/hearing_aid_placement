function [E,V] = Import_glstat(path)
% Function for importing the global energies reported in the glstat-file
% form LS-DYNA. 
%
% -------------------------------- INPUT -------------------------------- %
% path                          String containing the full path of the
%                               glstat-file
%
% -------------------------------- OUTPUT ------------------------------- %
% E                             Struct containing all the reported global
%                               energies and the time array.
%
% V                             3xN matrix with the blobal velocity in each
%                               direction. 
%
% ----------------------------------------------------------------------- %
%
% 05.07.2021 NIFD



%% Preallocate arrays

t = zeros(1,10000);
E_kin = t; E_int = t; E_sd = t; E_hg = t; E_d = t; E_se = t; W_e = t;
E_ero_kin = t; E_ero_int = t; E_ero_hg = t; E_tot = t; E_rat_ini = t;
E_rat = t; Vel = zeros(3,length(t));

%% Import data
% Open file path
fID = fopen(path);

% Read data line by line
index = 0;
tline = fgetl(fID);
while ischar(tline)
    % Identify if new time step block has been reached
    if contains(tline,'time.')
        index = index+1;
        t(index) = str2double(tline(end-11:end));
    end
    
    % Find all relevant energies and save in arrays
    if contains(tline,'kinetic energy') && ~contains(tline,'eroded')
        E_kin(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'internal energy') && ~contains(tline,'eroded')
        E_int(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'spring and damper energy')
        E_sd(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'hourglass energy') && ~contains(tline,'eroded')
        E_hg(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'system damping energy')
        E_d(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'sliding interface energy')
        E_se(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'external work')
        W_e(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'eroded kinetic energy')
        E_ero_kin(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'eroded internal energy')
        E_ero_int(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'eroded hourglass energy')
        E_ero_hg(index) = str2double(tline(end-11:end));
        
        
    elseif contains(tline,'total energy') && ~contains(tline,'initial')
        E_tot(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'total energy / initial energy')
        E_rat_ini(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'energy ratio w/o eroded energy')
        E_rat(index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'global x velocity')
        Vel(1,index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'global y velocity')
        Vel(2,index) = str2double(tline(end-11:end));
        
    elseif contains(tline,'global z velocity')
        Vel(3,index) = str2double(tline(end-11:end));
        
    end

    % Read next row of data
    tline = fgetl(fID);
    
end

% Close file
fclose(fID);

% Save variables
E.time = t(1:find(t,1,'last'));
E.Kinetic = E_kin(1:find(t,1,'last'));
E.Internal = E_int(1:find(t,1,'last'));
E.SpringDamper = E_sd(1:find(t,1,'last'));
E.Hourglass = E_hg(1:find(t,1,'last'));
E.Damping = E_d(1:find(t,1,'last'));
E.SlidingInterface = E_se(1:find(t,1,'last'));
E.ExternalWork = W_e(1:find(t,1,'last'));
E.ErodedKinetic = E_ero_kin(1:find(t,1,'last'));
E.ErodedInternal = E_ero_int(1:find(t,1,'last'));
E.ErodedHourglass = E_ero_hg(1:find(t,1,'last'));
E.Total = E_tot(1:find(t,1,'last'));
E.TotalToInitialRatio = E_rat_ini(1:find(t,1,'last'));
E.Ratio = E_rat(1:find(t,1,'last'));
V = Vel(:,1:find(t,1,'last'));
