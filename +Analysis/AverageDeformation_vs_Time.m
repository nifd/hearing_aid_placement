function [Disp,Rot] = AverageDeformation_vs_Time(Disp,ElMax)
% Function for processing the imported displacement data into a struct
% containing displacements and rotations in the geometric mean coordinate
% system of the body. The rotations are obtained by considering how a
% line between two nodes move during the displacement.
% -------------------------------- INPUT -------------------------------- %
% Disp                          Struct containing the imported displacement
%                               data. Must contain the fields:
%       (X,Y,Z)                 Directions of displacement
%               .Time           Array of sampled times
%               .Total          Array with node info and displacement
%                               along relevant axis. Contains 1 row for
%                               each node with the structure:
%                               [node# x0 y0 z0 [Disp]]
%                               Where [Disp] is the displacement array
%                               corresponding to .Time.
%
%
% ElMax                         Scalar indicating the maximum element size
%                               in the body. Used for tolerance for
%                               badefining the rotatio plane
% -------------------------------- OUTPUT ------------------------------- %
% Disp                          Struct updated with the following fields:
%               .Mean/Min/Max   Mean/Min/Max of the displacement array
%                               across all nodes.
%               (.Min/Max)Coord Min/Maximum Coordinate for the initial
%                               configuration
%               .MeanLoc        Mean between max and min coord for that dir
%
%       StartCoord              Startcorrdinates for all nodes in a
%                               coordinate system with origo at MeanLoc
%       TimeSeries              N_nodes x Ndirs x (Ntimes+1) array
%                               containing the displaced coordinates for
%                               all nodes at all times, including t=0
%
% Rot                           Struct with rotational data for the body
%                               considered. Fields:
%       (X,Y,Z)                 Axis of rotation, using right hand rule
%               .P/Q            Start stop point in the rotation plane for
%                               the considered line
%               .ind_(P/Q)      Index for nodes
%               .V              Vector from P to Q
%               .theta          Angle of rotation around Dir
%
% ----------------------------------------------------------------------- %

%% Basic displacement summaries

% Find directions available in the Disp Struct
DirNam = fields(Disp);
Ndirs = numel(DirNam);
Ntime = numel(Disp.X.Time);

for iDir = 1:Ndirs
    % Mean/Min/Max values of displacments
    Disp.(DirNam{iDir}).Mean = mean(Disp.(DirNam{iDir}).Total(:,5:end),1);
    Disp.(DirNam{iDir}).Min = min(Disp.(DirNam{iDir}).Total(:,5:end),1);
    Disp.(DirNam{iDir}).Max = max(Disp.(DirNam{iDir}).Total(:,5:end),1);
    
    % Min and max coordinates
    Disp.(DirNam{iDir}).MinCoord = min(Disp.(DirNam{iDir}).Total(:,iDir+1));
    Disp.(DirNam{iDir}).MaxCoord = max(Disp.(DirNam{iDir}).Total(:,iDir+1));
    
    % Geometric means
    Disp.(DirNam{iDir}).MeanLoc = (Disp.(DirNam{iDir}).MinCoord+Disp.(DirNam{iDir}).MaxCoord)/2;
    
end

%% Time Series of displacements

% Initialize arrays
Disp.StartCoord = Disp.X.Total(:,2:4) - [Disp.X.MeanLoc Disp.Y.MeanLoc Disp.Z.MeanLoc];
Disp.TimeSeries = zeros(size(Disp.StartCoord,1),size(Disp.StartCoord,2),Ntime+1);
Disp.TimeSeries(:,:,1) = Disp.StartCoord;

% Loop over time steps and directions
for iDir = 1:Ndirs
    for iTime = 1:Ntime
        % Displacements
        Disp.TimeSeries(:,iDir,iTime+1) = Disp.StartCoord(:,iDir) + Disp.(DirNam{iDir}).Total(:,4+iTime);
    end
end

%% Rotations

% Initialize arrays
P = zeros(2,Ntime+1); Q = P; V = P;
% Initialize angle array
theta = zeros(1,Ntime+1);

% Loop over time steps and directions
for iDir = 1:Ndirs
    % Indices of axes of plane rotating around current dir
    ind_rot = find(~strcmp(DirNam,DirNam{iDir}));
    
    % Nodes that lie in rotation plane
    range = mean(Disp.StartCoord(:,iDir)) + ElMax/2*[-1 1];
    ind_rot_plane = find(Disp.StartCoord(:,iDir) >= range(1) & Disp.StartCoord(:,iDir)<=range(2));
%     keyboard
    % Start (P) and end (Q) points at time = 0
    [P(1,1),ind_P] = min(Disp.TimeSeries(ind_rot_plane,ind_rot(1),1));
    P(2,1) = Disp.TimeSeries(ind_rot_plane(ind_P),ind_rot(2),1);
    
    [Q(1,1),ind_Q] = max(Disp.TimeSeries(ind_rot_plane,ind_rot(1),1));
    Q(2,1) = Disp.TimeSeries(ind_rot_plane(ind_Q),ind_rot(2),1);
    
    % Direction vector at time = 0
    V(:,1) = Q(:,1)-P(:,1);
    for iTime = 1:Ntime
        
        % Start (P) and end (Q) points at time = iTime + 1
        P(1,iTime+1) = Disp.TimeSeries(ind_rot_plane(ind_P),ind_rot(1),iTime+1);
        P(2,iTime+1) = Disp.TimeSeries(ind_rot_plane(ind_P),ind_rot(2),iTime+1);
        
        Q(1,iTime+1) = Disp.TimeSeries(ind_rot_plane(ind_Q),ind_rot(1),iTime+1);
        Q(2,iTime+1) = Disp.TimeSeries(ind_rot_plane(ind_Q),ind_rot(2),iTime+1);
        
        % Direction vector at time step iTime + 1
        V(:,iTime+1) = Q(:,iTime+1) - P(:,iTime+1);
        
        % calculate angle based on dot-product between original and current
        % direction vector
%         keyboard
        theta(iTime+1) = ...
            acosd((V(:,1)'*V(:,iTime+1))/(norm(V(:,1))*norm(V(:,iTime+1))));
    end
    
    % Save results in Rot struct
    Rot.(DirNam{iDir}).P = P;
    Rot.(DirNam{iDir}).ind_P = ind_rot_plane(ind_P);
    Rot.(DirNam{iDir}).ind_Q = ind_rot_plane(ind_Q);
    Rot.(DirNam{iDir}).Q = Q;
    Rot.(DirNam{iDir}).V = V;
    Rot.(DirNam{iDir}).theta = theta;
    clearvars P Q ind_P ind_Q V theta
end

