% Input file for evaluating the different kinds of loading of the
% instrument. Using the soft material for ear-rubber.
%
% 06.07.2021

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1;

DispPlotSelection = 'XZ';
RotPlotSelection = 'YZ';


% Friction values
FricVals = {'02','04','06'};
% FricVals = {'04'};
nFrics = numel(FricVals);

for iFric = 1:nFrics
    TypNames{iFric} = ['\mu = ' FricVals{iFric}];
end

%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
    RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end

DatFold = cell(nFrics,1);

for iFric = 1:nFrics
    DatFold{iFric} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric' FricVals{iFric} '_E72E5Pa_DampRat01_ID13_GaussForce\'];
    
    FoldCont = dir(DatFold{iFric});
    
    nFold = numel(FoldCont)-2;
    
    FoldNames = {FoldCont(3:end).name};
    
    for iFold = 1:nFold
        if FoldCont(iFold+2).isdir
            Dat.(FoldNames{iFold}) = [DatFold{iFric} FoldNames{iFold} '\'];
        else
            Dat.(FoldNames{iFold}) = [DatFold{iFric} FoldNames{iFold}];
        end
    end
    
    nDat = numel(fieldnames(Dat));
    
    
    % Displacement data
    if find(contains(fieldnames(Dat),'Disp'))
        FoldCont_Dir = dir(Dat.Disp);
        Dirs = cell(numel(FoldCont_Dir) - 2,1);
        Ndirs = numel(Dirs);
        DirNam = Dirs;
        for iDir = 1:Ndirs
            Dirs{iDir} = FoldCont_Dir(iDir+2).name;
            DirNam{iDir} = Dirs{iDir}(end);
        end
    else
        error(['Warning: No displacement data in folder ' DatFold{iFric} ' recorded - import will not work'])
    end
    
    %% Import energy data
    % Global energies - glstat file
    if find(contains(fieldnames(Dat),'glstat'))
        [glstat{iFric},V{iFric}] = ImportFunctions.Import_glstat(Dat.glstat);
    else
        disp('WARNING: No glstat-file in data directory - please include for global energy check')
    end
    
    % Material energies - matsum file
    if find(contains(fieldnames(Dat),'matsum'))
        [matsum{iFric}] = ImportFunctions.Import_matsum(Dat.matsum);
        fNam = fieldnames(matsum{iFric});
        PartNames{iFric} = fNam(~strcmp(fNam,'time'));
        nParts{iFric} = numel(PartNames{iFric});
    else
        disp('WARNING: No matsum-file in data directory - please include for part energy check')
    end
    
        %% Import boundary forces
    if find(contains(fieldnames(Dat),'bndout'))
        [t_RF{iFric},RF{iFric}] = ImportFunctions.Import_bndout(Dat.bndout);
    else
        disp('No boundary condition information data saved in folder')
    end
    
    
    
    %% Import Displacement data
    
    for iDir = 1:Ndirs
        [Disp{iFric}.(DirNam{iDir}).Time,Disp{iFric}.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]);
    end
    
    
    %% Import contact Forces
    ForceFoldDat = dir(Dat.ContactForces);
    ForceNames = {ForceFoldDat(3:end).name};
    nForce = numel(ForceNames);
    
    for iFile = 1:nForce
        ForceNames{iFile} = ForceNames{iFile}(1:end-4);
        Force{iFric}.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
    end
    
   
    %% Process data
    % Average and extrumum displacelemnts
    ElMax = 4e-4;
    [Disp{iFric},Rot{iFric}] = Analysis.AverageDeformation_vs_Time(Disp{iFric},ElMax);
    
    % Forces
    ForceNames{4} = 'Total';
    Force{iFric}.Total = zeros(size(Force{iFric}.SkinX));
    Force{iFric}.Total(:,1) = Force{iFric}.SkinX(:,1);
    for iForce = 1:nForce
        Force{iFric}.Total(:,2) = Force{iFric}.Total(:,2) + Force{iFric}.(ForceNames{iForce})(:,2).^2;
    end
    
    Force{iFric}.Total(:,2) = sqrt(Force{iFric}.Total(:,2));
    
    % glstat
    % Calculate norms
    glstat_names{iFric} = fieldnames(glstat{iFric});
    
    for iFields = 2:numel(glstat_names{iFric}) % time is the first field name
        glstat{iFric}.Norm.(glstat_names{iFric}{iFields}) = norm(glstat{iFric}.(glstat_names{iFric}{iFields}));
    end
    
end

%% Plotting


figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    hold on
    for iFric = 1:nFrics
        plot(Disp{iFric}.(DirNam{iDir}).Time,Disp{iFric}.(DirNam{iDir}).Mean*1e3)
        DispLeg{iFric} = ['\mu = ' num2str(str2double(FricVals{iFric})*1e-1)];
    end
    hold off
    ylabel(['u_' Dirs{iDir}(end) ', [mm]'])
    xlabel('Time, [s]')
    ylim([-1 1]*3)
    
    if iDir == 1
        legend(DispLeg,'Location','NW')
    end
    
    subplot(3,2,2*iDir)
    hold on
    for iFric = 1:nFrics
        plot([0 Disp{iFric}.(DirNam{iDir}).Time],Rot{iFric}.(DirNam{iDir}).theta)
    end
    hold off
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    xlabel('Time, [s]')
    ylim([-1 1]*10)
end




%%
fCol = 'rbkmc';
% Force
figure('Name','Boundary reaction force')
for iForce = 1:3
    
    subplot(3,1,iForce)
    hold on
    for iFric = 1:nFrics
        plot(t_RF{iFric},RF{iFric}(iForce,:),fCol(iFric))
    end
%     for iTime = 1:nTimes
%         plot(t_load(iTime,:),P(iTime,:),[fCol(iFric) '--'])
%     end
    hold off
    if iForce == 1
        legend(TypNames,'Location','E')
    end
end
xlabel('t, [s]')

figure('Name','Contact Force')
for iForce = 1:nForce+1
    
    
    subplot(2,2,iForce)
    hold on
    for iFric = 1:nFrics
        t = Force{iFric}.(ForceNames{iForce})(:,1);
        F = Force{iFric}.(ForceNames{iForce})(:,2);
        
        plot(t,F)
        if iForce < 4
            title(ForceNames{iForce})
        else
            title('Total')
        end
    end
    hold off
    if iForce == 1
        legend(DispLeg,'Location','NW')
    end
end

% Energy
for iFric = 1:nFrics
    eCount = 0;
    figure('Name',['Global energies for \mu = ' num2str(str2double(FricVals{iFric})*1e-1)])
    subplot 211
    hold on
    for iEne = 2:12 %From Ekin to Total
        if glstat{iFric}.Norm.(glstat_names{iFric}{iEne})/glstat{iFric}.Norm.Total >=1e-6
            eCount = eCount+1;
            plot(glstat{iFric}.time,glstat{iFric}.(glstat_names{iFric}{iEne}))
            leg{eCount} = glstat_names{iFric}{iEne};
        end
        
    end
    hold off
    ylabel('Energy, [mJ]')
    legend(leg,'Location','NW')
    set(gca,'FontSize',18)
    
    subplot 212
    hold on
    plot(glstat{iFric}.time,glstat{iFric}.(glstat_names{iFric}{13}))
    plot(glstat{iFric}.time,glstat{iFric}.(glstat_names{iFric}{14}),'--r')
    hold off
    ylabel('Energy ratio, [-]')
    xlabel('Time, [s]')
    ylim([0.95 1.05])
    legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')
    set(gca,'FontSize',18)
    
    
    
    figure('Name',['Part energies for \mu = ' num2str(str2double(FricVals{iFric})*1e-1)])
    for iPart = 1:nParts{iFric}
        eNams = fieldnames(matsum{iFric}.(PartNames{iFric}{iPart}).Energy);
        subplot(nParts{iFric},1,iPart)
        hold on
        for iEne = 1:numel(eNams)
            plot(matsum{iFric}.time,matsum{iFric}.(PartNames{iFric}{iPart}).Energy.(eNams{iEne}))
            leg{iEne} = eNams{iEne};
        end
        hold off
        legend(leg,'Location','EO')
        title(PartNames{iFric}{iPart});
        ylabel('Part Energy, [mJ]')
        if iPart == nParts{iFric}
            xlabel('Time, [s]')
        end
        set(gca,'FontSize',18)
    end
end
%% Selected displacement plots
figure('Name','Selected motion')
% Displacements
for iDir = 1:length(DispPlotSelection)
    subplot(2,2,2*iDir-1)
    hold on
    for iFric = 1:nFrics
        plot(Disp{iFric}.(DispPlotSelection(iDir)).Time,Disp{iFric}.(DispPlotSelection(iDir)).Mean*1e3)
    end
    hold off
    ylabel(['u_' DispPlotSelection(iDir) ', [mm]'])
    xlabel('Time, [s]')
    ylim([-3 1])
    set(gca,'FontSize',18)
    
    if iDir == 1
        legend(DispLeg,'Location','NE')
    end
    
    subplot(2,2,2*iDir)
    hold on
    for iFric = 1:nFrics
        plot([0 Disp{iFric}.(RotPlotSelection(iDir)).Time],Rot{iFric}.(RotPlotSelection(iDir)).theta)
    end
    hold off
    ylabel(['\theta_' DispPlotSelection(iDir) ', [^\circ]'])
    xlabel('Time, [s]')
    ylim([-4 8]*1)
    
    set(gca,'FontSize',18)
end


