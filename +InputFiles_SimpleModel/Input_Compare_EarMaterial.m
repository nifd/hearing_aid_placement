% Input file for evaluating different ear materials
% 06.07.2021

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1;

DispPlotSelection = 'XZ';
RotPlotSelection = 'YZ';

% Damping values
Materials = {'E36E5Pa','E72E5Pa','E144E5Pa'};
nTypes = numel(Materials);

TypNames = {'Soft Rubber','Medium Rubber','Hard Rubber'};


%% Applied load
n_load = 100;
nTimes = 1;

P = zeros(nTimes,n_load);
Pmax = 1;
c01 = sqrt(5e-6);
T_f = zeros(1,nTimes);
t_load = zeros(nTimes,n_load);
iTime = 1;
% for iTime = 1:nTimes
    T_f(iTime) = 10e-3;
    t_load(iTime,:) = linspace(0,T_f(iTime),n_load);
    % Gaussian Bell
    P_Gauss = Pmax*exp(-1/2*((t_load(iTime,:)-T_f(iTime)/2)/(c01*T_f(iTime)/0.01)).^2);
    % Half sine wave
    P_sin = sin(pi*t_load(iTime,:)/T_f(iTime));
    P(iTime,:) = P_Gauss.*P_sin;
% end

%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
    RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end

% Preallocate cell array for folder names
DatFold = cell(nTypes,1);

% Preallocate stuff
glstat = cell(nTypes,1);
V = glstat;
matsum = glstat;

t_RF = glstat;
RF = glstat;

Disp = glstat;
Force = glstat;
Rot = glstat;
for iMat = 1:nTypes
    % Data folder path
    DatFold{iMat} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_' Materials{iMat} '_DampRat01_ID13_GaussForce\'];
    % Content of folder
    FoldCont = dir(DatFold{iMat});
    if isempty(FoldCont)
        disp(['WARNING: Folder ' DatFold{iMat} ' does not exist - check naming'])
    end
    
    % Number of objects in folder
    nFold = numel(FoldCont)-2;
    % Names of objects
    FoldNames = {FoldCont(3:end).name};
    % Ensure that folder names have a backslash at the tail
    for iFold = 1:nFold
        if FoldCont(iFold+2).isdir
            Dat.(FoldNames{iFold}) = [DatFold{iMat} FoldNames{iFold} '\'];
        else
            Dat.(FoldNames{iFold}) = [DatFold{iMat} FoldNames{iFold}];
        end
    end
    
    % Displacement data - folder names
    if find(contains(fieldnames(Dat),'Disp'))
        FoldCont_Dir = dir(Dat.Disp);
        Dirs = cell(numel(FoldCont_Dir) - 2,1);
        Ndirs = numel(Dirs);
        DirNam = Dirs;
        for iDir = 1:Ndirs
            Dirs{iDir} = FoldCont_Dir(iDir+2).name;
            DirNam{iDir} = Dirs{iDir}(end);
        end
    else
        error(['Warning: No displacement data in folder ' DatFold{iMat} ' recorded - import will not work'])
    end
    
    %% Import energy data
    % Global energies - glstat file
    
    if find(contains(fieldnames(Dat),'glstat'))
        [glstat{iMat},V{iMat}] = ImportFunctions.Import_glstat(Dat.glstat);
    else
        disp('WARNING: No glstat-file in data directory - please include for global energy check')
    end
    
    % Material energies - matsum file
    if find(contains(fieldnames(Dat),'matsum'))
        [matsum{iMat}] = ImportFunctions.Import_matsum(Dat.matsum);
        fNam = fieldnames(matsum{iMat});
        PartNames{iMat} = fNam(~strcmp(fNam,'time'));
        nParts{iMat} = numel(PartNames{iMat});
    else
        disp('WARNING: No matsum-file in data directory - please include for part energy check')
    end
    
    %% Import boundary forces
    if find(contains(fieldnames(Dat),'bndout'))
        [t_RF{iMat},RF{iMat}] = ImportFunctions.Import_bndout(Dat.bndout);
    else
        disp('No boundary condition information data saved in folder')
    end
    
    
    
    %% Import Displacement data
    
    for iDir = 1:Ndirs
        [Disp{iMat}.(DirNam{iDir}).Time,Disp{iMat}.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]);
    end
    
    
    %% Import contact Forces
    ForceFoldDat = dir(Dat.ContactForces);
    ForceNames = {ForceFoldDat(3:end).name};
    nForce = numel(ForceNames);
    
    for iFile = 1:nForce
        ForceNames{iFile} = ForceNames{iFile}(1:end-4);
        Force{iMat}.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
    end
    
   
    %% Process data
    % Average and extrumum displacelemnts
    ElMax = 4e-4;
    [Disp{iMat},Rot{iMat}] = Analysis.AverageDeformation_vs_Time(Disp{iMat},ElMax);
    
    % Forces
    ForceNames{4} = 'Total';
    Force{iMat}.Total = zeros(size(Force{iMat}.SkinX));
    Force{iMat}.Total(:,1) = Force{iMat}.SkinX(:,1);
    for iForce = 1:nForce
        Force{iMat}.Total(:,2) = Force{iMat}.Total(:,2) + Force{iMat}.(ForceNames{iForce})(:,2).^2;
    end
    
    Force{iMat}.Total(:,2) = sqrt(Force{iMat}.Total(:,2));
    
    % glstat
    % Calculate norms
    glstat_names{iMat} = fieldnames(glstat{iMat});
    
    for iFields = 2:numel(glstat_names{iMat}) % time is the first field name
        glstat{iMat}.Norm.(glstat_names{iMat}{iFields}) = norm(glstat{iMat}.(glstat_names{iMat}{iFields}));
    end
    
end

%% Plotting
figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    hold on
    for iMat = 1:nTypes
        plot(Disp{iMat}.(DirNam{iDir}).Time,Disp{iMat}.(DirNam{iDir}).Mean*1e3)
    end
    hold off
    ylabel(['u_' Dirs{iDir}(end) ', [mm]'])
    xlabel('t, [s]')
    ylim([-1 1]*3)
    
    if iDir == 1
        legend(TypNames,'Location','NW')
    end
    
    subplot(3,2,2*iDir)
    hold on
    for iMat = 1:nTypes
        plot([0 Disp{iMat}.(DirNam{iDir}).Time],Rot{iMat}.(DirNam{iDir}).theta)
    end
    hold off
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    xlabel('t, [s]')
    ylim([-1 1]*10)
end



%%
fCol = 'rbkmc';
% Force
figure('Name','Boundary reaction force')
for iForce = 1:3
    
    subplot(3,1,iForce)
    hold on
    for iMat = 1:nTypes
        plot(t_RF{iMat},RF{iMat}(iForce,:),fCol(iMat))
    end
%     for iTime = 1:nTimes
        plot(t_load(iTime,:),P(iTime,:),[fCol(iMat) '--'])
%     end
    hold off
    if iForce == 1
        legend(TypNames,'Location','E')
    end
end
xlabel('t, [s]')

figure('Name','Contact Force')
for iForce = 1:nForce+1
    
    
    subplot(2,2,iForce)
    hold on
    for iMat = 1:nTypes
        t = Force{iMat}.(ForceNames{iForce})(:,1);
        F = Force{iMat}.(ForceNames{iForce})(:,2);
        plot(t,F,fCol(iMat))
    end
    %     for iType = 1:nTypes
    plot(t_load(iTime,:),P(iTime,:),[fCol(iMat) '--'])
    %     end
    
    
    hold off
    if iForce < 4
        title(ForceNames{iForce})
    else
        title('Total')
    end
    if iForce == 1
        legend(TypNames,'Location','NW')
    end
end

%%
% Energy
for iMat = 1:nTypes
    eCount = 0;
    figure('Name',['Global energies for ' TypNames{iMat}])
    subplot 211
    hold on
    for iEne = 2:12 %From Ekin to Total
        if glstat{iMat}.Norm.(glstat_names{iMat}{iEne})/glstat{iMat}.Norm.Total >=1e-6
            eCount = eCount+1;
            plot(glstat{iMat}.time,glstat{iMat}.(glstat_names{iMat}{iEne}))
            leg{eCount} = glstat_names{iMat}{iEne};
        end
        
    end
    hold off
    ylabel('Energy, [mJ]')
    legend(leg,'Location','NW')
    set(gca,'FontSize',18)
    
    subplot 212
    hold on
    plot(glstat{iMat}.time,glstat{iMat}.(glstat_names{iMat}{13}))
    plot(glstat{iMat}.time,glstat{iMat}.(glstat_names{iMat}{14}),'--r')
    hold off
    ylabel('Energy ratio, [-]')
    xlabel('t, [s]')
    ylim([0.95 1.05])
    legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')
    set(gca,'FontSize',18)
    
    
    
    figure('Name',['Part energies for ' TypNames{iMat}])
    for iPart = 1:nParts{iMat}
        eNams = fieldnames(matsum{iMat}.(PartNames{iMat}{iPart}).Energy);
        subplot(nParts{iMat},1,iPart)
        hold on
        for iEne = 1:numel(eNams)
            plot(matsum{iMat}.time,matsum{iMat}.(PartNames{iMat}{iPart}).Energy.(eNams{iEne}))
            leg{iEne} = eNams{iEne};
        end
        hold off
        legend(leg,'Location','EO')
        title(PartNames{iMat}{iPart});
        ylabel('Part Energy, [mJ]')
        if iPart == nParts{iMat}
            xlabel('t, [-]')
        end
        set(gca,'FontSize',18)
    end
end
%% Selected displacement plots
figure('Name','Selected motion')
% Displacements
for iDir = 1:length(DispPlotSelection)
    subplot(2,2,2*iDir-1)
    hold on
    for iMat = 1:nTypes
        plot(Disp{iMat}.(DispPlotSelection(iDir)).Time,Disp{iMat}.(DispPlotSelection(iDir)).Mean*1e3)
    end
    hold off
    ylabel(['u_' DispPlotSelection(iDir) ', [mm]'])
    xlabel('t, [s]')
    ylim([-3 1])
    set(gca,'FontSize',18)
    
    if iDir == 1
        legend(TypNames,'Location','NE')
    end
    
    subplot(2,2,2*iDir)
    hold on
    for iMat = 1:nTypes
        plot([0 Disp{iMat}.(RotPlotSelection(iDir)).Time],Rot{iMat}.(RotPlotSelection(iDir)).theta)
    end
    hold off
    ylabel(['\theta_' DispPlotSelection(iDir) ', [^\circ]'])
    xlabel('t, [s]')
    ylim([-4 8]*1)
    
    set(gca,'FontSize',18)
end


