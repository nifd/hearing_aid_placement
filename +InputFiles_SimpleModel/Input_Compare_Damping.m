% Input file for evaluating the different kinds of damping
% 06.07.2021

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1;

DispPlotSelection = 'XZ';
RotPlotSelection = 'YZ';

% Damping values
DampTypes = {'DampRat01','DampRat025','DampRat075','StiffDamp01','StiffDamp025'};
nTypes = numel(DampTypes);

TypNames = {'Material Damping Ratio 0.1','Material Damping Ratio 0.25','Material Damping Ratio 0.75','k-damping 0.1','k-damping 0.25'};


%% Applied load
n_load = 100;
nTimes = 1;

P = zeros(nTimes,n_load);
Pmax = 1;
c01 = sqrt(5e-6);
T_f = zeros(1,nTimes);
t_load = zeros(nTimes,n_load);
iTime = 1;
% for iTime = 1:nTimes
    T_f(iTime) = 10e-3;
    t_load(iTime,:) = linspace(0,T_f(iTime),n_load);
    % Gaussian Bell
    P_Gauss = Pmax*exp(-1/2*((t_load(iTime,:)-T_f(iTime)/2)/(c01*T_f(iTime)/0.01)).^2);
    % Half sine wave
    P_sin = sin(pi*t_load(iTime,:)/T_f(iTime));
    P(iTime,:) = P_Gauss.*P_sin;
% end

%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
    RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end

% Preallocate cell array for folder names
DatFold = cell(nTypes,1);

% Preallocate stuff
glstat = cell(nTypes,1);
V = glstat;
matsum = glstat;

t_RF = glstat;
RF = glstat;

Disp = glstat;
Force = glstat;
Rot = glstat;
for iDamp = 1:nTypes
    % Data folder path
    DatFold{iDamp} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_E72E5Pa_' DampTypes{iDamp} '_ID13_GaussForce\'];
    % Content of folder
    FoldCont = dir(DatFold{iDamp});
    if isempty(FoldCont)
        disp(['WARNING: Folder ' DatFold{iDamp} ' does not exist - check naming'])
    end
    
    % Number of objects in folder
    nFold = numel(FoldCont)-2;
    % Names of objects
    FoldNames = {FoldCont(3:end).name};
    % Ensure that folder names have a backslash at the tail
    for iFold = 1:nFold
        if FoldCont(iFold+2).isdir
            Dat.(FoldNames{iFold}) = [DatFold{iDamp} FoldNames{iFold} '\'];
        else
            Dat.(FoldNames{iFold}) = [DatFold{iDamp} FoldNames{iFold}];
        end
    end
    
    % Displacement data - folder names
    if find(contains(fieldnames(Dat),'Disp'))
        FoldCont_Dir = dir(Dat.Disp);
        Dirs = cell(numel(FoldCont_Dir) - 2,1);
        Ndirs = numel(Dirs);
        DirNam = Dirs;
        for iDir = 1:Ndirs
            Dirs{iDir} = FoldCont_Dir(iDir+2).name;
            DirNam{iDir} = Dirs{iDir}(end);
        end
    else
        error(['Warning: No displacement data in folder ' DatFold{iDamp} ' recorded - import will not work'])
    end
    
    %% Import energy data
    % Global energies - glstat file
    
    if find(contains(fieldnames(Dat),'glstat'))
        [glstat{iDamp},V{iDamp}] = ImportFunctions.Import_glstat(Dat.glstat);
    else
        disp('WARNING: No glstat-file in data directory - please include for global energy check')
    end
    
    % Material energies - matsum file
    if find(contains(fieldnames(Dat),'matsum'))
        [matsum{iDamp}] = ImportFunctions.Import_matsum(Dat.matsum);
        fNam = fieldnames(matsum{iDamp});
        PartNames{iDamp} = fNam(~strcmp(fNam,'time'));
        nParts{iDamp} = numel(PartNames{iDamp});
    else
        disp('WARNING: No matsum-file in data directory - please include for part energy check')
    end
    
    %% Import boundary forces
    if find(contains(fieldnames(Dat),'bndout'))
        [t_RF{iDamp},RF{iDamp}] = ImportFunctions.Import_bndout(Dat.bndout);
    else
        disp('No boundary condition information data saved in folder')
    end
    
    
    
    %% Import Displacement data
    
    for iDir = 1:Ndirs
        [Disp{iDamp}.(DirNam{iDir}).Time,Disp{iDamp}.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]);
    end
    
    
    %% Import contact Forces
    ForceFoldDat = dir(Dat.ContactForces);
    ForceNames = {ForceFoldDat(3:end).name};
    nForce = numel(ForceNames);
    
    for iFile = 1:nForce
        ForceNames{iFile} = ForceNames{iFile}(1:end-4);
        Force{iDamp}.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
    end
    
   
    %% Process data
    % Average and extrumum displacelemnts
    ElMax = 4e-4;
    [Disp{iDamp},Rot{iDamp}] = Analysis.AverageDeformation_vs_Time(Disp{iDamp},ElMax);
    
    % Forces
    ForceNames{4} = 'Total';
    Force{iDamp}.Total = zeros(size(Force{iDamp}.SkinX));
    Force{iDamp}.Total(:,1) = Force{iDamp}.SkinX(:,1);
    for iForce = 1:nForce
        Force{iDamp}.Total(:,2) = Force{iDamp}.Total(:,2) + Force{iDamp}.(ForceNames{iForce})(:,2).^2;
    end
    
    Force{iDamp}.Total(:,2) = sqrt(Force{iDamp}.Total(:,2));
    
    % glstat
    % Calculate norms
    glstat_names{iDamp} = fieldnames(glstat{iDamp});
    
    for iFields = 2:numel(glstat_names{iDamp}) % time is the first field name
        glstat{iDamp}.Norm.(glstat_names{iDamp}{iFields}) = norm(glstat{iDamp}.(glstat_names{iDamp}{iFields}));
    end
    
end

%% Plotting
figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    hold on
    for iDamp = 1:nTypes
        plot(Disp{iDamp}.(DirNam{iDir}).Time,Disp{iDamp}.(DirNam{iDir}).Mean*1e3)
    end
    hold off
    ylabel(['u_' Dirs{iDir}(end) ', [mm]'])
    xlabel('t, [s]')
    ylim([-1 1]*3)
    
    if iDir == 1
        legend(TypNames,'Location','NW')
    end
    
    subplot(3,2,2*iDir)
    hold on
    for iDamp = 1:nTypes
        plot([0 Disp{iDamp}.(DirNam{iDir}).Time],Rot{iDamp}.(DirNam{iDir}).theta)
    end
    hold off
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    xlabel('t, [s]')
    ylim([-1 1]*10)
end



%%
fCol = 'rbkmc';
% Force
figure('Name','Boundary reaction force')
for iForce = 1:3
    
    subplot(3,1,iForce)
    hold on
    for iDamp = 1:nTypes
        plot(t_RF{iDamp},RF{iDamp}(iForce,:),fCol(iDamp))
    end
%     for iTime = 1:nTimes
        plot(t_load(iTime,:),P(iTime,:),[fCol(iDamp) '--'])
%     end
    hold off
    if iForce == 1
        legend(TypNames,'Location','E')
    end
end
xlabel('t, [s]')

figure('Name','Contact Force')
for iForce = 1:nForce+1
    
    
    subplot(2,2,iForce)
    hold on
    for iDamp = 1:nTypes
        t = Force{iDamp}.(ForceNames{iForce})(:,1);
        F = Force{iDamp}.(ForceNames{iForce})(:,2);
        plot(t,F,fCol(iDamp))
    end
    %     for iType = 1:nTypes
    plot(t_load(iTime,:),P(iTime,:),[fCol(iDamp) '--'])
    %     end
    
    
    hold off
    if iForce < 4
        title(ForceNames{iForce})
    else
        title('Total')
    end
    if iForce == 1
        legend(TypNames,'Location','NW')
    end
end

%%
% Energy
for iDamp = 1:nTypes
    eCount = 0;
    figure('Name',['Global energies for ' TypNames{iDamp}])
    subplot 211
    hold on
    for iEne = 2:12 %From Ekin to Total
        if glstat{iDamp}.Norm.(glstat_names{iDamp}{iEne})/glstat{iDamp}.Norm.Total >=1e-6
            eCount = eCount+1;
            plot(glstat{iDamp}.time,glstat{iDamp}.(glstat_names{iDamp}{iEne}))
            leg{eCount} = glstat_names{iDamp}{iEne};
        end
        
    end
    hold off
    ylabel('Energy, [mJ]')
    legend(leg,'Location','NW')
    set(gca,'FontSize',18)
    
    subplot 212
    hold on
    plot(glstat{iDamp}.time,glstat{iDamp}.(glstat_names{iDamp}{13}))
    plot(glstat{iDamp}.time,glstat{iDamp}.(glstat_names{iDamp}{14}),'--r')
    hold off
    ylabel('Energy ratio, [-]')
    xlabel('t, [s]')
    ylim([0.95 1.05])
    legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')
    set(gca,'FontSize',18)
    
    
    
    figure('Name',['Part energies for ' TypNames{iDamp}])
    for iPart = 1:nParts{iDamp}
        eNams = fieldnames(matsum{iDamp}.(PartNames{iDamp}{iPart}).Energy);
        subplot(nParts{iDamp},1,iPart)
        hold on
        for iEne = 1:numel(eNams)
            plot(matsum{iDamp}.time,matsum{iDamp}.(PartNames{iDamp}{iPart}).Energy.(eNams{iEne}))
            leg{iEne} = eNams{iEne};
        end
        hold off
        legend(leg,'Location','EO')
        title(PartNames{iDamp}{iPart});
        ylabel('Part Energy, [mJ]')
        if iPart == nParts{iDamp}
            xlabel('t, [-]')
        end
        set(gca,'FontSize',18)
    end
end
%% Selected displacement plots
figure('Name','Selected motion')
% Displacements
for iDir = 1:length(DispPlotSelection)
    subplot(2,2,2*iDir-1)
    hold on
    for iDamp = 1:nTypes
        plot(Disp{iDamp}.(DispPlotSelection(iDir)).Time,Disp{iDamp}.(DispPlotSelection(iDir)).Mean*1e3)
    end
    hold off
    ylabel(['u_' DispPlotSelection(iDir) ', [mm]'])
    xlabel('t, [s]')
    ylim([-3 1])
    set(gca,'FontSize',18)
    
    if iDir == 1
        legend(TypNames,'Location','NE')
    end
    
    subplot(2,2,2*iDir)
    hold on
    for iDamp = 1:nTypes
        plot([0 Disp{iDamp}.(RotPlotSelection(iDir)).Time],Rot{iDamp}.(RotPlotSelection(iDir)).theta)
    end
    hold off
    ylabel(['\theta_' DispPlotSelection(iDir) ', [^\circ]'])
    xlabel('t, [s]')
    ylim([-4 8]*1)
    
    set(gca,'FontSize',18)
end


