% Input file for evaluating the different kinds of loading of the
% instrument. Using the soft material for ear-rubber. 
%
% 06.07.2021

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1; 

DispPlotSelection = 'XZ';
RotPlotSelection = 'YZ';

% Load Names
LoadNames = {'QuadDisp','CubeDisp','LinVel','QuadVel','GaussForce'};
nLoads = numel(LoadNames);
%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
     RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end

DatFold = cell(nLoads,1);
for iLoad = 1:nLoads
DatFold{iLoad} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_E36E5Pa_DampRat01_ID13_' LoadNames{iLoad} '\'];

FoldCont = dir(DatFold{iLoad});

nFold = numel(FoldCont)-2;

FoldNames = {FoldCont(3:end).name};

for iFold = 1:nFold
    if FoldCont(iFold+2).isdir
        Dat.(FoldNames{iFold}) = [DatFold{iLoad} FoldNames{iFold} '\'];
    else
        Dat.(FoldNames{iFold}) = [DatFold{iLoad} FoldNames{iFold}];
    end
end

nDat = numel(fieldnames(Dat));


% Displacement data
if find(contains(fieldnames(Dat),'Disp'))
    FoldCont_Dir = dir(Dat.Disp);
    Dirs = cell(numel(FoldCont_Dir) - 2,1);
    Ndirs = numel(Dirs);
    DirNam = Dirs;
    for iDir = 1:Ndirs
        Dirs{iDir} = FoldCont_Dir(iDir+2).name;
        DirNam{iDir} = Dirs{iDir}(end);
    end
else
    error(['Warning: No displacement data in folder ' DatFold{iLoad} ' recorded - import will not work'])
end

%% Import energy data
% Global energies - glstat file
if find(contains(fieldnames(Dat),'glstat'))
    [glstat{iLoad},V{iLoad}] = ImportFunctions.Import_glstat(Dat.glstat);
else
    disp('WARNING: No glstat-file in data directory - please include for global energy check')
end

% Material energies - matsum file
if find(contains(fieldnames(Dat),'matsum'))
    [matsum{iLoad}] = ImportFunctions.Import_matsum(Dat.matsum);
    fNam = fieldnames(matsum{iLoad});
    PartNames{iLoad} = fNam(~strcmp(fNam,'time'));
    nParts{iLoad} = numel(PartNames{iLoad});
else
    disp('WARNING: No matsum-file in data directory - please include for part energy check')
end

  %% Import boundary forces
    if find(contains(fieldnames(Dat),'bndout'))
        [t_RF{iLoad},RF{iLoad}] = ImportFunctions.Import_bndout(Dat.bndout);
    else
        disp('No boundary condition information data saved in folder')
    end
    


%% Import Displacement data

for iDir = 1:Ndirs
    [Disp{iLoad}.(DirNam{iDir}).Time,Disp{iLoad}.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]); 
end


%% Import contact Forces
ForceFoldDat = dir(Dat.ContactForces);
ForceNames = {ForceFoldDat(3:end).name};
nForce = numel(ForceNames);

for iFile = 1:nForce
    ForceNames{iFile} = ForceNames{iFile}(1:end-4);
    Force{iLoad}.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
end

%% Process data
% Average and extrumum displacelemnts
ElMax = 4e-4;
[Disp{iLoad},Rot{iLoad}] = Analysis.AverageDeformation_vs_Time(Disp{iLoad},ElMax);

% Forces
ForceNames{4} = 'Total';
Force{iLoad}.Total = zeros(size(Force{iLoad}.SkinX));
Force{iLoad}.Total(:,1) = Force{iLoad}.SkinX(:,1);
for iForce = 1:nForce
    Force{iLoad}.Total(:,2) = Force{iLoad}.Total(:,2) + Force{iLoad}.(ForceNames{iForce})(:,2).^2;
end

Force{iLoad}.Total(:,2) = sqrt(Force{iLoad}.Total(:,2));

% glstat
% Calculate norms
glstat_names{iLoad} = fieldnames(glstat{iLoad});

for iFields = 2:numel(glstat_names{iLoad}) % time is the first field name
    glstat{iLoad}.Norm.(glstat_names{iLoad}{iFields}) = norm(glstat{iLoad}.(glstat_names{iLoad}{iFields}));
end

end

%% Plotting

fCol = 'rbmkcyg'

figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    hold on
    for iLoad = 1:nLoads
        plot(Disp{iLoad}.(DirNam{iDir}).Time,Disp{iLoad}.(DirNam{iDir}).Mean*1e3)
    end
    hold off
    ylabel(['u_' Dirs{iDir}(end) ', [mm]'])
    xlabel('Time, [s]')
    ylim([-1 1]*3)
    
    if iDir == 1
        legend(LoadNames,'Location','NW')
    end
    
    subplot(3,2,2*iDir)
    hold on
    for iLoad = 1:nLoads
        plot([0 Disp{iLoad}.(DirNam{iDir}).Time],Rot{iLoad}.(DirNam{iDir}).theta)
    end
    hold off
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    xlabel('Time, [s]')
    ylim([-1 1]*10)
end


figure('Name','Boundary reaction force')
for iForce = 1:3
    
    subplot(3,1,iForce)
    hold on
    for iLoad = 1:nLoads
        plot(t_RF{iLoad},RF{iLoad}(iForce,:),fCol(iLoad))
    end
%     for iLoad = 1:nLoads
%         plot(t_load(iLoad,:),P(iLoad,:),[fCol(iLoad) '--'])
%     end
    hold off
    if iForce == 1
        legend(LoadNames,'Location','NE')
    end
end
xlabel('t/T_{sim}, [-]')


% Force
figure('Name','Contact Force')
for iForce = 1:nForce+1
    
    
    subplot(2,2,iForce)
    hold on
    for iLoad = 1:nLoads
    t = Force{iLoad}.(ForceNames{iForce})(:,1);
    F = Force{iLoad}.(ForceNames{iForce})(:,2);
    
    plot(t,F)
    if iForce < 4
        title(ForceNames{iForce})
    else
        title('Total')
    end
    end
    hold off
    if iForce == 1
        legend(LoadNames,'Location','NW')
    end
end

%%
for iLoad = 1:nLoads
eCount = 0;
figure('Name',['Global energies for ' LoadNames{iLoad}])
subplot 211
hold on
for iEne = 2:12 %From Ekin to Total
    if glstat{iLoad}.Norm.(glstat_names{iLoad}{iEne})/glstat{iLoad}.Norm.Total >=1e-6
        eCount = eCount+1;
        plot(glstat{iLoad}.time,glstat{iLoad}.(glstat_names{iLoad}{iEne}))
        leg{eCount} = glstat_names{iLoad}{iEne};
    end
    
end
hold off
ylabel('Energy, [mJ]')
legend(leg,'Location','NW')
set(gca,'FontSize',18)

subplot 212
hold on
plot(glstat{iLoad}.time,glstat{iLoad}.(glstat_names{iLoad}{13}))
plot(glstat{iLoad}.time,glstat{iLoad}.(glstat_names{iLoad}{14}),'--r')
hold off
ylabel('Energy ratio, [-]')
xlabel('Time, [s]')
ylim([0.95 1.05])
legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')
set(gca,'FontSize',18)



figure('Name',['Part energies for' LoadNames{iLoad}])
for iPart = 1:nParts{iLoad}
    eNams = fieldnames(matsum{iLoad}.(PartNames{iLoad}{iPart}).Energy);
    subplot(nParts{iLoad},1,iPart)
    hold on
    for iEne = 1:numel(eNams)
        plot(matsum{iLoad}.time,matsum{iLoad}.(PartNames{iLoad}{iPart}).Energy.(eNams{iEne}))
        leg{iEne} = eNams{iEne};
    end
    hold off
    legend(leg,'Location','EO')
    title(PartNames{iLoad}{iPart});
    ylabel('Part Energy, [mJ]')
    if iPart == nParts{iLoad}
        xlabel('Time, [s]')
    end
    set(gca,'FontSize',18)
end
end
%% Selected displacement plots
figure('Name','Selected motion')
% Displacements
for iDir = 1:length(DispPlotSelection)
    subplot(2,2,2*iDir-1)
    hold on
    for iLoad = 1:nLoads
        plot(Disp{iLoad}.(DispPlotSelection(iDir)).Time,Disp{iLoad}.(DispPlotSelection(iDir)).Mean*1e3)
    end
    hold off
    ylabel(['u_' DispPlotSelection(iDir) ', [mm]'])
    xlabel('Time, [s]')
    ylim([-3 1])
    set(gca,'FontSize',18)
    
    if iDir == 1
        legend(LoadNames,'Location','NE')
    end
    
    subplot(2,2,2*iDir)
    hold on
    for iLoad = 1:nLoads
        plot([0 Disp{iLoad}.(RotPlotSelection(iDir)).Time],Rot{iLoad}.(RotPlotSelection(iDir)).theta)
    end
    hold off
    ylabel(['\theta_' DispPlotSelection(iDir) ', [^\circ]'])
    xlabel('Time, [s]')
    ylim([-4 8]*1)
    
    set(gca,'FontSize',18)
end


