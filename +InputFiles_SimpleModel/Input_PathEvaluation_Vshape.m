% Input file for evaluating the positioning of a hearing aid. Plots the
% path of the centroid of surface of the instrument
% 27.04.2021 NIFD

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1; 

%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
     RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end
DatFold = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_E36E5Pa_DampRat01_ID13_CubeDisp\'];

FoldCont = dir(DatFold);

nFold = numel(FoldCont)-2;

FoldNames = {FoldCont(3:end).name};

for iFold = 1:nFold
    if FoldCont(iFold+2).isdir
        Dat.(FoldNames{iFold}) = [DatFold FoldNames{iFold} '\'];
    else
        Dat.(FoldNames{iFold}) = [DatFold FoldNames{iFold}];
    end
end

nDat = numel(fieldnames(Dat));



% Displacement data
if find(contains(fieldnames(Dat),'Disp'))
    FoldCont_Dir = dir(Dat.Disp);
    Dirs = cell(numel(FoldCont_Dir) - 2,1);
    Ndirs = numel(Dirs);
    DirNam = Dirs;
    for iDir = 1:Ndirs
        Dirs{iDir} = FoldCont_Dir(iDir+2).name;
        DirNam{iDir} = Dirs{iDir}(end);
    end
else
    error('Warning: No displacement data recorded - import will not work')
end

%% Import energy data
% Global energies - glstat file
if find(contains(fieldnames(Dat),'glstat'))
    [glstat,V] = ImportFunctions.Import_glstat(Dat.glstat);
else
    disp('WARNING: No glstat-file in data directory - please include for global energy check')
end

% Material energies - matsum file
if find(contains(fieldnames(Dat),'matsum'))
    [matsum] = ImportFunctions.Import_matsum(Dat.matsum);
    fNam = fieldnames(matsum);
    PartNames = fNam(~strcmp(fNam,'time'));
    nParts = numel(PartNames);
else
    disp('WARNING: No matsum-file in data directory - please include for part energy check')
end



%% Import Displacement data

for iDir = 1:Ndirs
    [Disp.(DirNam{iDir}).Time,Disp.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]); 
end


%% Import contact Forces
ForceFoldDat = dir(Dat.ContactForces);
ForceNames = {ForceFoldDat(3:end).name};
nForce = numel(ForceNames);

for iFile = 1:nForce
    ForceNames{iFile} = ForceNames{iFile}(1:end-4);
    Force.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
end



%% Process data
% Average and extrumum displacelemnts
ElMax = 4e-4;
[Disp,Rot] = Analysis.AverageDeformation_vs_Time(Disp,ElMax);

% Forces
ForceNames{4} = 'Total';
Force.Total = zeros(size(Force.SkinX));
Force.Total(:,1) = Force.SkinX(:,1);
for iForce = 1:nForce
    Force.Total(:,2) = Force.Total(:,2) + Force.(ForceNames{iForce})(:,2).^2;
end

Force.Total(:,2) = sqrt(Force.Total(:,2));

% glstat
% Calculate norms
glstat_names = fieldnames(glstat);

for iFields = 2:numel(glstat_names) % time is the first field name
    glstat.Norm.(glstat_names{iFields}) = norm(glstat.(glstat_names{iFields}));
end



%% Plotting

figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    plot(Disp.(DirNam{iDir}).Time,Disp.(DirNam{iDir}).Mean*1e3)
    ylabel([Dirs{iDir}(end) ', [mm]'])
    
    subplot(3,2,2*iDir)
    plot([0 Disp.(DirNam{iDir}).Time],Rot.(DirNam{iDir}).theta)
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    
end
xlabel('Time, [s]')



% Force
figure('Name','Contact Force')
for iForce = 1:nForce+1
    t = Force.(ForceNames{iForce})(:,1);
    F = Force.(ForceNames{iForce})(:,2);
    
    subplot(2,2,iForce)
    plot(t,F)
    if iForce < 4
        title(ForceNames{iForce})
    else
        title('Total')
    end
end

% glstat
if EnergyPlot == 1

eCount = 0;
figure('Name','Global energies')
subplot 211
hold on
for iEne = 2:12 %From Ekin to Total
    if glstat.Norm.(glstat_names{iEne})/glstat.Norm.Total >=1e-6
        eCount = eCount+1;
        plot(glstat.time,glstat.(glstat_names{iEne}))
        leg{eCount} = glstat_names{iEne};
    end
    
end
hold off
ylabel('Energy, [J]')
legend(leg,'Location','NW')

subplot 212
hold on
plot(glstat.time,glstat.(glstat_names{13}))
plot(glstat.time,glstat.(glstat_names{14}),'--r')
hold off
ylabel('Energy ratio, [-]')
xlabel('Time, [s]')
ylim([0.95 1.05])
legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')


%% matsum
figure('Name','Part energies')
for iPart = 1:nParts
    eNams = fieldnames(matsum.(PartNames{iPart}).Energy);
    subplot(nParts,1,iPart)
    hold on
    for iEne = 1:numel(eNams)
        plot(matsum.time,matsum.(PartNames{iPart}).Energy.(eNams{iEne}))
        leg{iEne} = eNams{iEne};
    end
    hold off
    legend(leg,'Location','EO')
    title(PartNames{iPart});
    ylabel('Part Energy, [J]')
    if iPart == nParts
        xlabel('Time, [s]')
    end
end

end

%% Animation


% figure
% for iTime = 1:length(Disp.X.Time)+1
%    plot3(Disp.TimeSeries(:,3,iTime),Disp.TimeSeries(:,2,iTime),Disp.TimeSeries(:,1,iTime),'.')
%    axis equal
%    axis([Disp.Z.MinCoord-Disp.Z.MeanLoc Disp.Z.MaxCoord-Disp.Z.MeanLoc Disp.Y.MinCoord-Disp.Y.MeanLoc Disp.Y.MaxCoord-Disp.Y.MeanLoc Disp.X.MinCoord-Disp.X.MeanLoc Disp.X.MaxCoord-Disp.X.MeanLoc])
% xlabel('Z')
% ylabel('Y')
% zlabel('X')
%    pause(0.15)
% end


% figure
% plot3(Disp.StartCoord(:,3),Disp.StartCoord(:,2),Disp.StartCoord(:,1),'.')
% hold on
% 
% hold off
% axis equal
% xlabel('Z')
% ylabel('Y')
% zlabel('X')
% % view([0 -1 0])
