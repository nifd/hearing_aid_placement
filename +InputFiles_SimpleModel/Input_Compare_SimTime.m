% Input file for evaluating the different kinds of loading of the
% instrument. Using the soft material for ear-rubber.
%
% 06.07.2021

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1;

DispPlotSelection = 'XZ';
RotPlotSelection = 'YZ';


% Load times 
SimTimes = {'','_T10ms_InitContact','_T30ms','_T50ms'};
SimNames = {'10 ms','10 ms, Initial Contact','30 ms','50 ms'};

nTimes = numel(SimTimes);

%% Applied load
n_load = 100;

P = zeros(nTimes,n_load);
Pmax = 1;
c01 = sqrt(5e-6);
T_f = zeros(1,nTimes);
t_load = zeros(3,n_load);
for iTime = 1:nTimes
    T_f(iTime) = str2double(SimNames{iTime}(1:2))*1e-3;
    t_load(iTime,:) = linspace(0,T_f(iTime),n_load);
    % Gaussian Bell
    P_Gauss = Pmax*exp(-1/2*((t_load(iTime,:)-T_f(iTime)/2)/(c01*T_f(iTime)/0.01)).^2);
    % Half sine wave
    P_sin = sin(pi*t_load(iTime,:)/T_f(iTime));
    P(iTime,:) = P_Gauss.*P_sin;
end

%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
    RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end

DatFold = cell(nTimes,1);

for iTime = 1:nTimes
    DatFold{iTime} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_E72E5Pa_DampRat01_ID13_GaussForce' SimTimes{iTime} '\'];
  
    FoldCont = dir(DatFold{iTime});
    
    if isempty(FoldCont)
        disp(['WARNING: Folder ' DatFold{iTime} ' does not exist - check naming'])
    end
    
    nFold = numel(FoldCont)-2;
    
    FoldNames = {FoldCont(3:end).name};

    for iFold = 1:nFold
        if FoldCont(iFold+2).isdir
            Dat.(FoldNames{iFold}) = [DatFold{iTime} FoldNames{iFold} '\'];
        else
            Dat.(FoldNames{iFold}) = [DatFold{iTime} FoldNames{iFold}];
        end
    end
    
    nDat = numel(fieldnames(Dat));
    
    
    % Displacement data
    if find(contains(fieldnames(Dat),'Disp'))
        FoldCont_Dir = dir(Dat.Disp);
        Dirs = cell(numel(FoldCont_Dir) - 2,1);
        Ndirs = numel(Dirs);
        DirNam = Dirs;
        for iDir = 1:Ndirs
            Dirs{iDir} = FoldCont_Dir(iDir+2).name;
            DirNam{iDir} = Dirs{iDir}(end);
        end
    else
        error(['Warning: No displacement data in folder ' DatFold{iTime} ' recorded - import will not work'])
    end
    
    %% Import energy data
    % Global energies - glstat file
    if find(contains(fieldnames(Dat),'glstat'))
        [glstat{iTime},V{iTime}] = ImportFunctions.Import_glstat(Dat.glstat);
    else
        disp('WARNING: No glstat-file in data directory - please include for global energy check')
    end
    
    % Material energies - matsum file
    if find(contains(fieldnames(Dat),'matsum'))
        [matsum{iTime}] = ImportFunctions.Import_matsum(Dat.matsum);
        fNam = fieldnames(matsum{iTime});
        PartNames{iTime} = fNam(~strcmp(fNam,'time'));
        nParts{iTime} = numel(PartNames{iTime});
    else
        disp('WARNING: No matsum-file in data directory - please include for part energy check')
    end
    
    %% Import boundary forces
    if find(contains(fieldnames(Dat),'bndout'))
        [t_RF{iTime},RF{iTime}] = ImportFunctions.Import_bndout(Dat.bndout);
    else
        disp('No boundary condition information data saved in folder')
    end
    
    
    
    %% Import Displacement data
    
    for iDir = 1:Ndirs
        [Disp{iTime}.(DirNam{iDir}).Time,Disp{iTime}.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]);
    end
    
    
    %% Import contact Forces
    ForceFoldDat = dir(Dat.ContactForces);
    ForceNames = {ForceFoldDat(3:end).name};
    nForce = numel(ForceNames);
    
    for iFile = 1:nForce
        ForceNames{iFile} = ForceNames{iFile}(1:end-4);
        Force{iTime}.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
    end
    
   
    %% Process data
    % Average and extrumum displacelemnts
    ElMax = 4e-4;
    [Disp{iTime},Rot{iTime}] = Analysis.AverageDeformation_vs_Time(Disp{iTime},ElMax);
    
    % Forces
    ForceNames{4} = 'Total';
    Force{iTime}.Total = zeros(size(Force{iTime}.SkinX));
    Force{iTime}.Total(:,1) = Force{iTime}.SkinX(:,1);
    for iForce = 1:nForce
        Force{iTime}.Total(:,2) = Force{iTime}.Total(:,2) + Force{iTime}.(ForceNames{iForce})(:,2).^2;
    end
    
    Force{iTime}.Total(:,2) = sqrt(Force{iTime}.Total(:,2));
    
    % glstat
    % Calculate norms
    glstat_names{iTime} = fieldnames(glstat{iTime});
    
    for iFields = 2:numel(glstat_names{iTime}) % time is the first field name
        glstat{iTime}.Norm.(glstat_names{iTime}{iFields}) = norm(glstat{iTime}.(glstat_names{iTime}{iFields}));
    end
    
end

%% Plotting
T_f = ones(1,4);


figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    hold on
    for iTime = 1:nTimes
        plot(Disp{iTime}.(DirNam{iDir}).Time/T_f(iTime),Disp{iTime}.(DirNam{iDir}).Mean*1e3)
    end
    hold off
    ylabel(['u_' Dirs{iDir}(end) ', [mm]'])
    xlabel('t/T_{sim}, [-]')
    ylim([-1 1]*3)
    
    if iDir == 1
        legend(SimNames,'Location','NW')
    end
    
    subplot(3,2,2*iDir)
    hold on
    for iTime = 1:nTimes
        plot([0 Disp{iTime}.(DirNam{iDir}).Time]/T_f(iTime),Rot{iTime}.(DirNam{iDir}).theta)
    end
    hold off
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    xlabel('t/T_{sim}, [s]')
    ylim([-1 1]*10)
end



%%
fCol = 'rbkm';
% Force
figure('Name','Boundary reaction force')
for iForce = 1:3
    
    subplot(3,1,iForce)
    hold on
    for iTime = 1:nTimes
        plot(t_RF{iTime}/T_f(iTime),RF{iTime}(iForce,:),fCol(iTime))
    end
    for iTime = 1:nTimes
        plot(t_load(iTime,:),P(iTime,:),[fCol(iTime) '--'])
    end
    hold off
    if iForce == 1
        legend(SimNames,'Location','E')
    end
end
xlabel('t/T_{sim}, [-]')

figure('Name','Contact Force')
for iForce = 1:nForce+1
    
    
    subplot(2,2,iForce)
    hold on
    for iTime = 1:nTimes
        t = Force{iTime}.(ForceNames{iForce})(:,1);
        F = Force{iTime}.(ForceNames{iForce})(:,2);
        plot(t/T_f(iTime),F,fCol(iTime))
    end
    for iTime = 1:nTimes
        plot(t_load(iTime,:),P(iTime,:),[fCol(iTime) '--'])
    end
    
    
    hold off
    if iForce < 4
            title(ForceNames{iForce})
        else
            title('Total')
        end
    if iForce == 1
        legend(SimNames,'Location','NW')
    end
end

%%
% Energy
for iTime = 1:nTimes
    eCount = 0;
    figure('Name',['Global energies for T = ' SimNames{iTime}])
    subplot 211
    hold on
    for iEne = 2:12 %From Ekin to Total
        if glstat{iTime}.Norm.(glstat_names{iTime}{iEne})/glstat{iTime}.Norm.Total >=1e-6
            eCount = eCount+1;
            plot(glstat{iTime}.time/T_f(iTime),glstat{iTime}.(glstat_names{iTime}{iEne}))
            leg{eCount} = glstat_names{iTime}{iEne};
        end
        
    end
    hold off
    ylabel('Energy, [mJ]')
    legend(leg,'Location','NW')
    set(gca,'FontSize',18)
    
    subplot 212
    hold on
    plot(glstat{iTime}.time/T_f(iTime),glstat{iTime}.(glstat_names{iTime}{13}))
    plot(glstat{iTime}.time/T_f(iTime),glstat{iTime}.(glstat_names{iTime}{14}),'--r')
    hold off
    ylabel('Energy ratio, [-]')
    xlabel('t/T_{sim}, [-]')
    ylim([0.95 1.05])
    legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')
    set(gca,'FontSize',18)
    
    
    
    figure('Name',['Part energies for T = ' SimNames{iTime}])
    for iPart = 1:nParts{iTime}
        eNams = fieldnames(matsum{iTime}.(PartNames{iTime}{iPart}).Energy);
        subplot(nParts{iTime},1,iPart)
        hold on
        for iEne = 1:numel(eNams)
            plot(matsum{iTime}.time/T_f(iTime),matsum{iTime}.(PartNames{iTime}{iPart}).Energy.(eNams{iEne}))
            leg{iEne} = eNams{iEne};
        end
        hold off
        legend(leg,'Location','EO')
        title(PartNames{iTime}{iPart});
        ylabel('Part Energy, [mJ]')
        if iPart == nParts{iTime}
            xlabel('t/T_{sim}, [-]')
        end
        set(gca,'FontSize',18)
    end
end
%% Selected displacement plots
figure('Name','Selected motion')
% Displacements
for iDir = 1:length(DispPlotSelection)
    subplot(2,2,2*iDir-1)
    hold on
    for iTime = 1:nTimes
        plot(Disp{iTime}.(DispPlotSelection(iDir)).Time/T_f(iTime),Disp{iTime}.(DispPlotSelection(iDir)).Mean*1e3)
    end
    hold off
    ylabel(['u_' DispPlotSelection(iDir) ', [mm]'])
    xlabel('t/T_{sim}, [-]')
    ylim([-3 1])
    set(gca,'FontSize',18)
    
    if iDir == 1
        legend(SimNames,'Location','NE')
    end
    
    subplot(2,2,2*iDir)
    hold on
    for iTime = 1:nTimes
        plot([0 Disp{iTime}.(RotPlotSelection(iDir)).Time]/T_f(iTime),Rot{iTime}.(RotPlotSelection(iDir)).theta)
    end
    hold off
    ylabel(['\theta_' DispPlotSelection(iDir) ', [^\circ]'])
    xlabel('t/T_{sim}, [-]')
    ylim([-4 8]*1)
    
    set(gca,'FontSize',18)
end


