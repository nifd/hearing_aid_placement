% Input file for evaluating the different kinds of loading of the
% instrument. Using the soft material for ear-rubber.
%
% 06.07.2021

% Clear Workspace
close all; clearvars; clc;

%% Input parameters

% Plot the global and part-by-part energies in the model
EnergyPlot = 1;

DispPlotSelection = 'XZ';
RotPlotSelection = 'YZ';

% Elements and formulations
ElTypes = {'ID13','ID4','ID16','Flexible','Mesh refinement'};
nTypes = numel(ElTypes);

TypNames = {'ID13 - Rigid','ID4 - Rigid','ID16 - Rigid','ID13 - Flexible','ID13 - Mesh refinement'};


%% Applied load
n_load = 100;
nTimes = 1;

P = zeros(nTimes,n_load);
Pmax = 1;
c01 = sqrt(5e-6);
T_f = zeros(1,nTimes);
t_load = zeros(nTimes,n_load);
iTime = 1;
% for iTime = 1:nTimes
    T_f(iTime) = 10e-3;
    t_load(iTime,:) = linspace(0,T_f(iTime),n_load);
    % Gaussian Bell
    P_Gauss = Pmax*exp(-1/2*((t_load(iTime,:)-T_f(iTime)/2)/(c01*T_f(iTime)/0.01)).^2);
    % Half sine wave
    P_sin = sin(pi*t_load(iTime,:)/T_f(iTime));
    P(iTime,:) = P_Gauss.*P_sin;
% end

%% Data Path
% Root data folder
ComputerName = getenv('computername');

if strcmp(ComputerName,'DK-C-KBN-NIFD-1')
    RootDat = 'D:\OneDrive - Demant\';
elseif strcmp(ComputerName,'DK-C-KBN-NIFP')
    RootDat = 'C:\Users\nifd\OneDrive - Demant Group Services\';
else
    disp('Running on non-NIFD PC. Data path might not be correct - please check')
end

DatFold = cell(nTypes,1);

for iType = 1:nTypes
    if strcmp(ElTypes{iType},'Flexible')
        DatFold{iType} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\FlexHI_StatFric02_E72E5Pa_DampRat01_ID13_GaussForce\'];
    elseif strcmp(ElTypes{iType},'Mesh refinement')
        DatFold{iType} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_E72E5Pa_DampRat01_ID13_GaussForce_Refined\'];
    else
        DatFold{iType} = [RootDat 'SpecialistTasks\Hearing Aid Placement\MatLab\Data\RigidHI_StatFric02_E72E5Pa_DampRat01_' ElTypes{iType} '_GaussForce\'];
    end
    


    FoldCont = dir(DatFold{iType});
    
    if isempty(FoldCont)
        disp(['WARNING: Folder ' DatFold{iType} ' does not exist - check naming'])
    end
    
    nFold = numel(FoldCont)-2;
    
    FoldNames = {FoldCont(3:end).name};

    for iFold = 1:nFold
        if FoldCont(iFold+2).isdir
            Dat.(FoldNames{iFold}) = [DatFold{iType} FoldNames{iFold} '\'];
        else
            Dat.(FoldNames{iFold}) = [DatFold{iType} FoldNames{iFold}];
        end
    end
    
    nDat = numel(fieldnames(Dat));
    
    
    % Displacement data
    if find(contains(fieldnames(Dat),'Disp'))
        FoldCont_Dir = dir(Dat.Disp);
        Dirs = cell(numel(FoldCont_Dir) - 2,1);
        Ndirs = numel(Dirs);
        DirNam = Dirs;
        for iDir = 1:Ndirs
            Dirs{iDir} = FoldCont_Dir(iDir+2).name;
            DirNam{iDir} = Dirs{iDir}(end);
        end
    else
        error(['Warning: No displacement data in folder ' DatFold{iType} ' recorded - import will not work'])
    end
    
    %% Import energy data
    % Global energies - glstat file
    if find(contains(fieldnames(Dat),'glstat'))
        [glstat{iType},V{iType}] = ImportFunctions.Import_glstat(Dat.glstat);
    else
        disp('WARNING: No glstat-file in data directory - please include for global energy check')
    end
    
    % Material energies - matsum file
    if find(contains(fieldnames(Dat),'matsum'))
        [matsum{iType}] = ImportFunctions.Import_matsum(Dat.matsum);
        fNam = fieldnames(matsum{iType});
        PartNames{iType} = fNam(~strcmp(fNam,'time'));
        nParts{iType} = numel(PartNames{iType});
    else
        disp('WARNING: No matsum-file in data directory - please include for part energy check')
    end
    
    %% Import boundary forces
    if find(contains(fieldnames(Dat),'bndout'))
        [t_RF{iType},RF{iType}] = ImportFunctions.Import_bndout(Dat.bndout);
    else
        disp('No boundary condition information data saved in folder')
    end
    
    
    
    %% Import Displacement data
    
    for iDir = 1:Ndirs
        [Disp{iType}.(DirNam{iDir}).Time,Disp{iType}.(DirNam{iDir}).Total] = ImportFunctions.ImportNodalDisplacementTimeSeries([Dat.Disp Dirs{iDir}]);
    end
    
    
    %% Import contact Forces
    ForceFoldDat = dir(Dat.ContactForces);
    ForceNames = {ForceFoldDat(3:end).name};
    nForce = numel(ForceNames);
    
    for iFile = 1:nForce
        ForceNames{iFile} = ForceNames{iFile}(1:end-4);
        Force{iType}.(ForceNames{iFile}) = ImportFunctions.ImportEnergy([Dat.ContactForces ForceNames{iFile}]);
    end
    
   
    %% Process data
    % Average and extrumum displacelemnts
    ElMax = 4e-4;
    [Disp{iType},Rot{iType}] = Analysis.AverageDeformation_vs_Time(Disp{iType},ElMax);
    
    % Forces
    ForceNames{4} = 'Total';
    Force{iType}.Total = zeros(size(Force{iType}.SkinX));
    Force{iType}.Total(:,1) = Force{iType}.SkinX(:,1);
    for iForce = 1:nForce
        Force{iType}.Total(:,2) = Force{iType}.Total(:,2) + Force{iType}.(ForceNames{iForce})(:,2).^2;
    end
    
    Force{iType}.Total(:,2) = sqrt(Force{iType}.Total(:,2));
    
    % glstat
    % Calculate norms
    glstat_names{iType} = fieldnames(glstat{iType});
    
    for iFields = 2:numel(glstat_names{iType}) % time is the first field name
        glstat{iType}.Norm.(glstat_names{iType}{iFields}) = norm(glstat{iType}.(glstat_names{iType}{iFields}));
    end
    
end

%% Plotting
T_f = ones(1,5);


figure('Name','All motion')
% Displacements
for iDir = 1:Ndirs
    subplot(3,2,2*iDir-1)
    hold on
    for iType = 1:nTypes
        plot(Disp{iType}.(DirNam{iDir}).Time/T_f(iType),Disp{iType}.(DirNam{iDir}).Mean*1e3)
    end
    hold off
    ylabel(['u_' Dirs{iDir}(end) ', [mm]'])
    xlabel('t/T_{sim}, [-]')
    ylim([-1 1]*3)
    
    if iDir == 1
        legend(TypNames,'Location','NW')
    end
    
    subplot(3,2,2*iDir)
    hold on
    for iType = 1:nTypes
        plot([0 Disp{iType}.(DirNam{iDir}).Time]/T_f(iType),Rot{iType}.(DirNam{iDir}).theta)
    end
    hold off
    ylabel(['\theta_' Dirs{iDir}(end) ', [^\circ]'])
    xlabel('t/T_{sim}, [s]')
    ylim([-1 1]*10)
end



%%
fCol = 'rbkmg';
% Force
figure('Name','Boundary reaction force')
for iForce = 1:3
    
    subplot(3,1,iForce)
    hold on
    for iType = 1:nTypes
        plot(t_RF{iType}/T_f(iType),RF{iType}(iForce,:),fCol(iType))
    end
%     for iTime = 1:nTimes
        plot(t_load(iTime,:),P(iTime,:),[fCol(iType) '--'])
%     end
    hold off
    if iForce == 1
        legend(TypNames,'Location','E')
    end
end
xlabel('t/T_{sim}, [-]')

figure('Name','Contact Force')
for iForce = 1:nForce+1
    
    
    subplot(2,2,iForce)
    hold on
    for iType = 1:nTypes
        t = Force{iType}.(ForceNames{iForce})(:,1);
        F = Force{iType}.(ForceNames{iForce})(:,2);
        plot(t/T_f(iType),F,fCol(iType))
    end
%     for iType = 1:nTypes
        plot(t_load(iTime,:),P(iTime,:),[fCol(iType) '--'])
%     end
    
    
    hold off
    if iForce < 4
            title(ForceNames{iForce})
        else
            title('Total')
        end
    if iForce == 1
        legend(TypNames,'Location','NW')
    end
end

%%
% Energy
for iType = 1:nTypes
    eCount = 0;
    figure('Name',['Global energies for ' TypNames{iType}])
    subplot 211
    hold on
    for iEne = 2:12 %From Ekin to Total
        if glstat{iType}.Norm.(glstat_names{iType}{iEne})/glstat{iType}.Norm.Total >=1e-6
            eCount = eCount+1;
            plot(glstat{iType}.time/T_f(iType),glstat{iType}.(glstat_names{iType}{iEne}))
            leg{eCount} = glstat_names{iType}{iEne};
        end
        
    end
    hold off
    ylabel('Energy, [mJ]')
    legend(leg,'Location','NW')
    set(gca,'FontSize',18)
    
    subplot 212
    hold on
    plot(glstat{iType}.time/T_f(iType),glstat{iType}.(glstat_names{iType}{13}))
    plot(glstat{iType}.time/T_f(iType),glstat{iType}.(glstat_names{iType}{14}),'--r')
    hold off
    ylabel('Energy ratio, [-]')
    xlabel('t/T_{sim}, [-]')
    ylim([0.95 1.05])
    legend('E_{tot}/E_{ini}','E_{tot}/(E_{ini}+W_e)')
    set(gca,'FontSize',18)
    
    
    
    figure('Name',['Part energies for ' TypNames{iType}])
    for iPart = 1:nParts{iType}
        eNams = fieldnames(matsum{iType}.(PartNames{iType}{iPart}).Energy);
        subplot(nParts{iType},1,iPart)
        hold on
        for iEne = 1:numel(eNams)
            plot(matsum{iType}.time/T_f(iType),matsum{iType}.(PartNames{iType}{iPart}).Energy.(eNams{iEne}))
            leg{iEne} = eNams{iEne};
        end
        hold off
        legend(leg,'Location','EO')
        title(PartNames{iType}{iPart});
        ylabel('Part Energy, [mJ]')
        if iPart == nParts{iType}
            xlabel('t/T_{sim}, [-]')
        end
        set(gca,'FontSize',18)
    end
end
%% Selected displacement plots
figure('Name','Selected motion')
% Displacements
for iDir = 1:length(DispPlotSelection)
    subplot(2,2,2*iDir-1)
    hold on
    for iType = 1:nTypes
        plot(Disp{iType}.(DispPlotSelection(iDir)).Time/T_f(iType),Disp{iType}.(DispPlotSelection(iDir)).Mean*1e3)
    end
    hold off
    ylabel(['u_' DispPlotSelection(iDir) ', [mm]'])
    xlabel('t, [s]')
    ylim([-3 1])
    set(gca,'FontSize',18)
    
    if iDir == 1
        legend(TypNames,'Location','NE')
    end
    
    subplot(2,2,2*iDir)
    hold on
    for iType = 1:nTypes
        plot([0 Disp{iType}.(RotPlotSelection(iDir)).Time]/T_f(iType),Rot{iType}.(RotPlotSelection(iDir)).theta)
    end
    hold off
    ylabel(['\theta_' DispPlotSelection(iDir) ', [^\circ]'])
    xlabel('t, [s]')
    ylim([-4 8]*1)
    
    set(gca,'FontSize',18)
end

%% Visualize motion of all points

% VisTypes = [1 4];
% iCol = 'br';
% count = 0;
% figure('Name','Instrument motion')
% 
% 
% for iTime = 1:length(Disp{1}.X.Time)+1
%     for iType = 1:length(VisTypes)
%         hold on
%         TypIndex = VisTypes(iType);
%         plot3(Disp{TypIndex}.TimeSeries(:,3,iTime),Disp{TypIndex}.TimeSeries(:,2,iTime),Disp{TypIndex}.TimeSeries(:,1,iTime),[iCol(iType) '.'])
%         axis equal
%         axis([Disp{TypIndex}.Z.MinCoord-Disp{TypIndex}.Z.MeanLoc Disp{TypIndex}.Z.MaxCoord-Disp{TypIndex}.Z.MeanLoc ...
%             Disp{TypIndex}.Y.MinCoord-Disp{TypIndex}.Y.MeanLoc Disp{TypIndex}.Y.MaxCoord-Disp{TypIndex}.Y.MeanLoc ...
%             Disp{TypIndex}.X.MinCoord-Disp{TypIndex}.X.MeanLoc Disp{TypIndex}.X.MaxCoord-Disp{TypIndex}.X.MeanLoc]*2)
%         hold off
%     end
%     xlabel('Z')
%     ylabel('Y')
%     zlabel('X')
%     view([0 -1 0])
%     pause(0.2)
% end

